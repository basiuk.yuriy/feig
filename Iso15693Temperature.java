import de.feig.fedm.*;
import de.feig.fedm.exception.FedmRuntimeException;
import de.feig.fedm.taghandler.ThBase;
import de.feig.fedm.taghandler.ThIso15693_TI;
import de.feig.fedm.types.DataBuffer;

public class Iso15693Temperature {
    public static void main(String[] args) {

        // Create Reader Module with Request Mode UniDirectional = Advanced Protocol
        // Using try with to auto close the ReaderModule
        try(ReaderModule reader = new ReaderModule(RequestMode.UniDirectional))
        {
            // Scan connected USB Reader
            int scan = UsbManager.startDiscover();
            if (scan > 0)
            {
                System.out.println("Scan USB-Devices: " + scan + " FEIG Reader");
            } else
            {
                System.out.println("No FEIG Reader found.");
                return;
            }
            // Create Connector Object (USB) by using the UsbManager
            Connector usbConnector = UsbManager.popDiscover().connector();
            // Connect Reader
            System.out.println("Start connection with Reader: " + usbConnector.usbDeviceId() + "...");
            reader.connect(usbConnector);
            // Error handling
            if (reader.lastError() != ErrorCode.Ok)
            {
                System.out.println("Error while Connecting: " + reader.lastError());
                System.out.println(reader.lastErrorText());
                return;
            }
            // Output ReaderType
            System.out.println("Reader " + reader.info().readerTypeToString() + " connected.\n");

            // Main cycle of Tags handler. Will  be convert to thread.
            int state = 0;
            long counter = 0;
            while (counter < 2000000)
            {
                counter++;
                state = reader.hm().inventory();
                if (state < ErrorCode.Ok)
                {

                }
                else
                {
                    if (state > ErrorCode.Ok)
                    {
                        System.out.println("Status : " + reader.lastStatusText());
                    }
                    else
                    {
                        System.out.println("inventory: " + ErrorCode.toString(state));

                        long cnt = reader.hm().itemCount();
                        for (long i = 0; i < cnt; i++)
                        {
                            String tID = "";
                            TagItem tagItem = reader.hm().tagItem((long)i);
                            tID = tagItem.iddToHexString();
                            System.out.println("New TAG received. ID -> " + tID);



                            ThBase th = reader.hm().createTagHandler(tagItem);

                            ThIso15693_TI thIso15693_ti = (ThIso15693_TI)th;

                            // ******************************************************
                            // Send Transparent command
                            // ******************************************************

                            // Write request Option
                            byte mode = 0x2;

                            //Length of the Transponder response in bit without SOF and EOF. During write operations
                            //REP-LENGTH is depending on ERROR_FLAG in the Transponder response:
                            //- 4 Byte if ERROR_FLAG is “1”.
                            //- – REP-LENGTH if ERROR_FLAG is “0”
                            long rspBitLen = 0x100;

                            // reserved (CMD-RSP-DELAY)
                            long cmdRspDelay = 0;

                            //EOF-PULSE-DELAY:
                            //EOF Pulse delay is used in write operations with ISO15693 write option “1”. EOF to define
                            //the in response delay for Transponder response (ISO15693: t1)
                            //e.g. ISO15693 maximum value: 0x846A * 590ns = 20ms
                            long eofPulseDelay = 0x14;

                            //Multiple 302µs grids (only for option 5)
                            long multiGrids = 0;

                            // Config Tag command.
                            byte[] bCmd = new byte[11];
                            bCmd[0] = 66;
                            bCmd[1] = 33;
                            bCmd[2] = 2;
                            bCmd[3] = 0;
                            bCmd[4] = 0;
                            bCmd[5] = 0;
                            bCmd[6] = 0;
                            bCmd[7] = 0;
                            bCmd[8] = 0;
                            bCmd[9] = 0;
                            bCmd[10] = 0;

                            DataBuffer reqBuffer = new DataBuffer(bCmd);

                            //@RpcRefParam DataBuffer rspData;
                            DataBuffer rspBuffer = new DataBuffer();

                            int res = thIso15693_ti.transparentCommand(mode, rspBitLen, cmdRspDelay, eofPulseDelay, multiGrids, reqBuffer, rspBuffer);

                            if (res == 0) {
                                System.out.println("The command has been sent successfully");
                            }
                            else
                            {
                                String errorWriteBlocks = "";

                                if (res < 0) {
                                    errorWriteBlocks = ErrorCode.toString(res);
                                }
                                else
                                    errorWriteBlocks = ReaderStatus.toString(res);
                                String finalErrorWriteBlocks = errorWriteBlocks;
                                System.out.println("Error transpanrentCommand "+ finalErrorWriteBlocks);
                            }


                        }
                    }
                }
            }

            reader.disconnect();
            if (reader.lastError() == ErrorCode.Ok)
            {
                System.out.println("\n" + reader.info().readerTypeToString() + " disconnected.");
            }
        }
        catch (FedmRuntimeException e)
        {
            System.out.println(e.getMessage());
        }
    }
}
